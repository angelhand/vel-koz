import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.util.List;

/**
 * nvidia-smi实体
 *
 * @author angelhand
 * @date 2023/11/1
 */
@JacksonXmlRootElement(localName = "nvidia_smi_log")
@JsonIgnoreProperties(ignoreUnknown = true)
public class NvidiaSmiLog {
    @JacksonXmlProperty(localName = "driver_version")
    private String driverVersion;

    @JacksonXmlProperty(localName = "cuda_version")
    private String cudaVersion;

    @JacksonXmlProperty(localName = "gpu")
    private List<GPU> gpus;

    public String getDriverVersion() {
        return driverVersion;
    }

    public void setDriverVersion(String driverVersion) {
        this.driverVersion = driverVersion;
    }

    public String getCudaVersion() {
        return cudaVersion;
    }

    public void setCudaVersion(String cudaVersion) {
        this.cudaVersion = cudaVersion;
    }

    public List<GPU> getGpus() {
        return gpus;
    }

    public void setGpus(List<GPU> gpus) {
        this.gpus = gpus;
    }
}
