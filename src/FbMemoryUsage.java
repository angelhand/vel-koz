import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

/**
 * nvidia-smi 内存使用实体
 *
 * @author angelhand
 * @date 2023/11/1
 */
@JacksonXmlRootElement(localName = "fb_memory_usage")
public class FbMemoryUsage {
    @JacksonXmlProperty(localName = "total")
    private int total;

    @JacksonXmlProperty(localName = "used")
    private int used;

    @JacksonXmlProperty(localName = "free")
    private int free;

    public int getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = Integer.parseInt(total.substring(0, total.indexOf(" ")));
    }

    public int getUsed() {
        return used;
    }

    public void setUsed(String used) {
        this.used = Integer.parseInt(used.substring(0, used.indexOf(" ")));
    }

    public int getFree() {
        return free;
    }

    public void setFree(String free) {
        this.free = Integer.parseInt(free.substring(0, free.indexOf(" ")));
    }
}
