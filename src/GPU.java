import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

/**
 * nvidia-smi gpu实体
 *
 * @author angelhand
 * @date 2023/11/1
 */
@JacksonXmlRootElement(localName = "gpu")
@JsonIgnoreProperties(ignoreUnknown = true)
public class GPU {
    @JacksonXmlProperty(isAttribute = true, localName = "id")
    private String id;

    @JacksonXmlProperty(localName = "fb_memory_usage")
    private FbMemoryUsage fbMemoryUsage;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public FbMemoryUsage getFbMemoryUsage() {
        return fbMemoryUsage;
    }

    public void setFbMemoryUsage(FbMemoryUsage fbMemoryUsage) {
        this.fbMemoryUsage = fbMemoryUsage;
    }
}
