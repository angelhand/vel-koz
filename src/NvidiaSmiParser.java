import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.PumpStreamHandler;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

/**
 * nvidia-smi命令解析为java对象
 * 需要安装好相应的环境
 *
 * @author angelhand
 * @date 2023/11/1
 */
public class NvidiaSmiParser {
    private static XmlMapper xmlMapper = XmlMapper.builder()
            .defaultUseWrapper(false)
            .build();

    /**
     * 检查是否有英伟达驱动
     *
     * @return true/false
     */
    public static boolean haveNvidia() throws IOException {
        try (ByteArrayOutputStream std = new ByteArrayOutputStream();
             ByteArrayOutputStream err = new ByteArrayOutputStream()) {

            CommandLine commandLine = new CommandLine("nvidia-smi");
            DefaultExecutor exec = new DefaultExecutor();
            PumpStreamHandler streamHandler = new PumpStreamHandler(std, err);
            exec.setStreamHandler(streamHandler);

            int exitCode = exec.execute(commandLine);
            if (exitCode != 0) {
                return false;
            } else {
                return true;
            }
        }
    }

    /**
     * 获取英伟达显卡信息
     *
     * @return 显卡信息
     */
    public static NvidiaSmiLog parse() throws IOException {
        NvidiaSmiLog result = null;

        try (ByteArrayOutputStream std = new ByteArrayOutputStream();
             ByteArrayOutputStream err = new ByteArrayOutputStream()) {

            CommandLine commandLine = new CommandLine("nvidia-smi -q -x");
            DefaultExecutor exec = new DefaultExecutor();
            PumpStreamHandler streamHandler = new PumpStreamHandler(std, err);
            exec.setStreamHandler(streamHandler);

            int exitCode = exec.execute(commandLine);
            if (exitCode != 0) {
                return result;
            }

            result = xmlMapper.readValue(std.toString(), NvidiaSmiLog.class);
        }
        return result;
    }

    /**
     * 只获取gpu信息
     *
     * @return gpu信息
     */
    public static List<GPU> parseGpu() throws IOException {
        List<GPU> result = null;
        NvidiaSmiLog nvidiaSmiLog = parse();
        if (nvidiaSmiLog == null) {
            return result;
        }

        result = nvidiaSmiLog.getGpus();
        return result;
    }
}
