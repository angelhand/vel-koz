使用java获取英伟达显卡信息，包括GPU数量和对应显存信息。

基于nvidia-smi工具，使用Java进行进程调用获取相关信息并进行解析得到。

使用示例：
```java
List<GPU> gpus = NvidiaSmiParser.parseGpu();
for (GPU gpu : gpus) {
    // 获取显卡号
    System.out.println(gpu.getId());
    // 获取当前卡总显存
    System.out.println(gpu.getFbMemoryUsage().getTotal());
    // 获取当前卡剩余显存
    System.out.println(gpu.getFbMemoryUsage().getFree());
    // 获取当前卡已用显存
    System.out.println(gpu.getFbMemoryUsage().getUsed());
}
```
